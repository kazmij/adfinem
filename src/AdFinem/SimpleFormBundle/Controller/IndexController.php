<?php

namespace AdFinem\SimpleFormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use AdFinem\SimpleFormBundle\Controller\MainController;
use APY\DataGridBundle\Grid\Source\Entity;
use APY\DataGridBundle\Grid\Column\TextColumn;
use APY\DataGridBundle\Grid\Column\ActionsColumn;
use APY\DataGridBundle\Grid\Action\MassAction;
use APY\DataGridBundle\Grid\Action\DeleteMassAction;
use APY\DataGridBundle\Grid\Action\RowAction;
use APY\DataGridBundle\Grid\Grid;
use APY\DataGridBundle\Grid\Row;
use AdFinem\SimpleFormBundle\Entity\People;
use AdFinem\SimpleFormBundle\Entity\Attachment;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class IndexController extends MainController {

    /**
     * @Template()
     * @param Request $request
     */
    public function indexAction(Request $request) {
        $source = new Entity('AdFinemSimpleFormBundle:People'); //create grid source
        // Get a grid instance
        /* @var $grid Grid */
        $grid = $this->get('grid');

        // Set the source
        $grid->setSource($source); //set grid source

        $actions = array();

        // Attach a rowAction to the Actions Column
        $actions['rowActionEdit'] = new RowAction($this->translator->trans('Edit'), 'person_edit');
        $actions['rowActionEdit']->setAttributes(array('class' => "fa fa-pencil-square-o"));

        $actions['rowActionShow'] = new RowAction($this->translator->trans('Show'), 'person_show');
        $actions['rowActionShow']->setAttributes(array('class' => "fa fa-eye"));

        $actions['rowActionDelete'] = new RowAction($this->translator->trans('Delete'), 'person_delete');
        $actions['rowActionDelete']->setAttributes(array('class' => "fa fa-times confirmAction", 'data-confirm-title' => $this->translator->trans('Confirm delete action'), 'data-confirm-content' => $this->translator->trans('Confirm delete action')));

        $actionsColumn = new ActionsColumn('actions', $this->translator->trans('Action'), $actions, '');
        $actionsColumn->setFilterable(false);
        $grid->addColumn($actionsColumn);

        // Set the selector of the number of items per page
        $grid->setLimits(array(5, 10, 15, 20, 30, 50, 100, 200));

        // Set the default page
        $grid->setDefaultPage(1);

        $grid->setDefaultLimit(20);

        return $grid->getGridResponse();
    }

    /**
     * Creates a new People entity.
     *
     * @Template()
     * @param Request $request
     */
    public function newAction(Request $request) {
        $person = new People();
        $attachment = new Attachment(); //initialize one attachment on start
        $person->addAttachment($attachment);

        $form = $this->createForm('AdFinem\SimpleFormBundle\Form\PeopleType', $person); //create form
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) { //if form submitted and valid
            $this->em->persist($person);
            $this->em->flush();
            //show flash success 
            $this->setFlash(sprintf($this->translator->trans('Entry #%s successfully been added!'), $person->getId()), 'success');

            return $this->redirectToRoute('person_show', array('id' => $person->getId()));
        } elseif ($form->isSubmitted()) { //only if submitted and not valid, shwo error
            $this->setFlash($this->translator->trans('Form contain errors!'), 'danger');
        }

        $this->return['form'] = $form->createView();
        $this->return['person'] = $person;
        $this->return['title'] = $this->translator->trans('Create new Person');

        return $this->return;
    }

    /**
     * Finds and displays a People entity.
     *
     * @Template()
     * @param Request $request
     * @param People $person
     */
    public function showAction(Request $request, People $person) {
        $this->return['person'] = $person;

        $this->return['title'] = sprintf($this->translator->trans('Show Person details with lastname: "%s"'), $person->getLastname());

        return $this->return;
    }

    /**
     * Displays a form to edit an existing People entity.
     *
     * @Template()
     * @param Request $request
     * @param People $person
     */
    public function editAction(Request $request, People $person) {
        $editForm = $this->createForm('AdFinem\SimpleFormBundle\Form\PeopleType', $person);
        $editForm->remove('attachments'); //remove attachemnts field - not used in edit form
        $editForm->handleRequest($request); //handle data from request

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            // Check entry has changed
            $uow = $this->em->getUnitOfWork();
            $uow->computeChangeSets();

            if ($uow->isEntityScheduled($person)) { //if no changed
                $this->setFlash(sprintf($this->translator->trans('Entry #%s successfully been updated!'), $person->getId()), 'success');
            } else { //if changed
                $this->setFlash(sprintf($this->translator->trans('Nothing changed in form!'), $person->getId()), 'danger');
            }
            $this->em->flush();
            return $this->redirectToRoute('person_edit', array('id' => $person->getId())); //redirect to edit (the same action)
        } elseif ($editForm->isSubmitted()) {
            $this->setFlash($this->translator->trans('Form contain errors!'), 'danger');
        }

        $this->return['edit_form'] = $editForm->createView();
        $this->return['person'] = $person;
        $this->return['title'] = sprintf($this->translator->trans('Edit Person with lastname: "%s"'), $person->getLastname());

        return $this->return;
    }

    /**
     * Deletes a People entity.
     *
     * @param Request $request
     * @param People $person
     */
    public function deleteAction(Request $request, People $person) {
        $form = $this->createDeleteForm($person);
        $form->submit($request); //emulate form send

        if ($form->isSubmitted() && $form->isValid()) {
            $id = $person->getId(); //store entity id
            $this->em->remove($person); //remvoe entity
            $this->em->flush();
            $this->setFlash(sprintf($this->translator->trans('Entry #%s successfully been removed!'), $id), 'success');
        } elseif ($form->isSubmitted()) {
            $this->setFlash($this->translator->trans('An error occured, try again later!'), 'danger');
        }

        return $this->redirectToRoute('ad_finem_simple_form_homepage'); //redirect to start page
    }

    /**
     * Creates a form to delete a People entity.
     *
     * @param People $person The People entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(People $person) {
        return $this->createFormBuilder(null, array('csrf_protection' => false)) //remove csrf token from delete form
                        ->setAction($this->generateUrl('person_delete', array('id' => $person->getId())))
                        ->setMethod('DELETE')
                        ->getForm();
    }

    /**
     * Download file action
     * 
     * @param Request $request
     * @param Attachment $attachment
     * @return BinaryFileResponse
     */
    public function downloadAction(Request $request, Attachment $attachment) {
        $path = $this->get('kernel')->getRootDir() . "/../web/";
        $file = $path . $attachment->getWebPath();
        if (!file_exists($file)) {
            $this->setFlash(sprintf($this->translator->trans('File %s no exist!'), $attachment->getPath()), 'danger');
            return $this->redirectToRoute('person_show', array('id' => $attachment->getPerson()->getId()));
        }
        
        BinaryFileResponse::trustXSendfileTypeHeader();
        $response = new BinaryFileResponse($file);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);

        return $response;
    }

}
