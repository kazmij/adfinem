<?php

namespace AdFinem\SimpleFormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Bundle\FrameworkBundle\Routing\Router;

class MainController extends Controller {

    /**
     * Return array
     * @var Array
     */
    protected $return = array();

    /**
     * EntityManager isntance
     * @var EntityManager
     */
    protected $em;

    /**
     * \Doctrine\DBAL\Connection isntance
     * @var \Doctrine\DBAL\Connection
     */
    protected $db;

    /**
     *
     * @var Request
     */
    protected $request;

    /**
     * @var TimedTwigEngine
     */
    protected $renderer;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Translator
     */
    protected $translator;

    public function _init() {
        $this->request = $this->container->get('request');
        $this->em = $this->container->get('doctrine')->getManager('default');
        $this->renderer = $this->container->get('templating');
        $this->session = $this->container->get('session');
        $this->router = $this->container->get('router');
        $this->translator = $this->container->get('translator');
    }

    /**
     * 
     * @param string $msg
     * @param string $type
     */
    public function setFlash($msg, $type) {
        $this->request->getSession()->getFlashBag()->add(
                $type, $msg
        );
    }

}
