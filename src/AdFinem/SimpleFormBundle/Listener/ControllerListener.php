<?php

namespace AdFinem\SimpleFormBundle\Listener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Controller listener
 * Do init method before all other controller methods if exist init function
 */
class ControllerListener {

    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }

    public function onKernelController(FilterControllerEvent $event) {

        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        $controllerObject = $controller[0];

        if ($controllerObject instanceof ExceptionController) {
            return;
        }

        if (in_array('_init', get_class_methods($controllerObject))) {
            $controllerObject->_init($event->getRequest());
        }
    }

    public function onKernelResponse(FilterResponseEvent $event) {
        
    }

}
