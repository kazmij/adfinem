<?php

namespace AdFinem\SimpleFormBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToLocalizedStringTransformer;

class AppExtension extends \Twig_Extension {

    /**
     * @var Container
     */
    private $container;

    public function __construct(Container $container) {
        $this->container = $container;
    }

    public function getName() {
        return 'app_extension';
    }

    public function getFilters() {
        return array(
            'format_datetime' => new \Twig_Filter_Method($this, 'format_datetime'),
        );
    }

    public function getFunctions() {
        return array(
            'file_exists' => new \Twig_Function_Method($this, 'fileExists'),
        );
    }

    public function format_datetime(\DateTime $datetime) {
        try {
            $transformer = new DateTimeToLocalizedStringTransformer();
            $value = $transformer->transform($datetime);
        } catch (\Exception $e) {
            $value = $datetime->format('Y-m-d H:i');
        }
        return $value;
    }

    public function fileExists($file) {
        return file_exists($file);
    }


}
