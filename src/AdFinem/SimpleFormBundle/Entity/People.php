<?php

namespace AdFinem\SimpleFormBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations
use APY\DataGridBundle\Grid\Mapping as GRID;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * People
 *
 * @ORM\Table(name="people")
 * @ORM\Entity(repositoryClass="AdFinem\SimpleFormBundle\Repository\PeopleRepository")
 * @UniqueEntity("email")
 * @ORM\HasLifecycleCallbacks()
 * @GRID\Source(columns="id, lastname, email, attachments.id:count, created, updated", groupBy={"id"})
 * 
 */
class People {

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * 
     * @GRID\Column(field="id", title="ID")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=10, nullable=true)
     * @Assert\Length(
     *      max = 10
     * )
     * 
     * @GRID\Column(field="name", title="Name")
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(name="lastname", type="string", length=60)
     * @Assert\NotBlank()
     * 
     * @GRID\Column(field="lastname", title="Lastname")
     */
    protected $lastname;

    /**
     * @var string
     * 
     * Validate Email and hostname with  "checkHost"
     * 
     * @ORM\Column(name="email", type="string", length=100)
     * @Assert\NotBlank()
     * @Assert\Email(
     *      checkHost = true
     * )
     * 
     * @GRID\Column(field="email", title="Email", unique=true)
     */
    protected $email;

    /**
     * Attachment
     * @ORM\OneToMany(targetEntity="Attachment", cascade={"persist"}, mappedBy="person")
     * 
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "You must specify at least one",
     *      max = "5",
     *      maxMessage = "You must specify five for maximum"
     * )
     * @Assert\Valid 
     * 
     * @GRID\Column(field="attachments.id:count", title="Images count")
     */
    protected $attachments;

    /**
     * Auto datetime insert on create
     * 
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     * 
     * @GRID\Column(field="created", type="datetime", title="Created at")
     */
    protected $created;

    /**
     * Auto change datetime on update
     * 
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     * 
     * @GRID\Column(field="updated", type="datetime", title="Updated at")
     */
    protected $updated;

    /**
     * Constructor
     */
    public function __construct() {
        $this->attachments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return People
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return People
     */
    public function setLastname($lastname) {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname() {
        return $this->lastname;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return People
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Add attachments
     *
     * @param \AdFinem\SimpleFormBundle\Entity\Attachment $attachment
     * @return People
     */
    public function addAttachment(\AdFinem\SimpleFormBundle\Entity\Attachment $attachment) {
        $this->attachments[] = $attachment;
        $attachment->setPerson($this);

        return $this;
    }

    /**
     * Remove attachments
     *
     * @param \AdFinem\SimpleFormBundle\Entity\Attachment $attachment
     */
    public function removeAttachment(\AdFinem\SimpleFormBundle\Entity\Attachment $attachment) {
        $this->attachments->removeElement($attachment);
    }

    /**
     * Get attachments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAttachments() {
        return $this->attachments;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return People
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return People
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @Assert\Callback
     * 
     * Callback validate, if name start with "A" then lastname cant start with the same letter
     * Its working only for capital letter "A", add modifier "i" to reaction for "A" and "a" in regulat expressions
     */
    public function validate(ExecutionContextInterface $context) {
        $regex = '/^A.*/';
        if ($this->name && preg_match($regex, $this->name)) {
            if ($this->lastname && preg_match($regex, $this->lastname)) {
                $context->buildViolation('This name and lastname can\' start with "A"!')
                        ->atPath('lastname')
                        ->addViolation();
            }
        }
    }

    /**
     * @ORM\PreRemove()
     */
    public function removeAttachments() {
        foreach ($this->attachments as $attachment){
            @unlink($attachment->getWebPath());
        }
    }

}
