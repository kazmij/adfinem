<?php

namespace AdFinem\SimpleFormBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AdFinem\SimpleFormBundle\Form\AttachmentType;

class PeopleType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name')
                ->add('lastname')
                ->add('email')
                ->add('attachments', CollectionType::class, array(
                    'entry_type' => AttachmentType::class,
                    'by_reference' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'error_bubbling' => true
                ))
                ->add('submit', 'submit', array(
                    'label' => ' Save',
                    'attr' => array(
                        'class' => 'btn btn-success fa fa-floppy-o'
                    )
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AdFinem\SimpleFormBundle\Entity\People'
        ));
    }

}
